<h1>OAT technical exercise</h1>
<h2>by Fernando Delgado</h2>

<h3>Requeriments</h3>
* PHP 7.4
* Composer 2

<h3>Installation</h3>
$ composer update

<h4>Set up</h4>
Modify .env file to your needs:

* REPOSITORY: which file the system will use

  json -> handles json file  
  csv -> handles csv file

* SOURCE_LANGUAGE: which language the system will translate from (English by default).
  https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
