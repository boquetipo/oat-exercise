<?php

namespace App\Exceptions;

use Exception;

class FileAccessException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            "Error while adding a question",
            500
        );
    }
}


