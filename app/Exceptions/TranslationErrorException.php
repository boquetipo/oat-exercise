<?php

namespace App\Exceptions;

use Exception;

class TranslationErrorException extends Exception
{
    public function __construct(int $code)
    {
        parent::__construct(
            'Text could not be translated. Please try again.',
            $code
        );
    }
}
