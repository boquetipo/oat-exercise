<?php

namespace App\Http\Controllers;

use App\Exceptions\TranslationErrorException;
use App\Services\RepositoryHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    protected $repositoryHandler;

    public function __construct(RepositoryHandler $handler)
    {
        $this->repositoryHandler = $handler;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): Response
    {
        $result = $this->repositoryHandler->getQuestions($request->get('lang'));

        return response(['data' => $result]);
    }

    /**
     * Store a newly created resource in repository.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $response = $this->checkInputData($data);

        if (null !== $response) {
            return $response;
        }

        try {
            $this->repositoryHandler->addQuestion($data);
        } catch (TranslationErrorException $exception) {
            return response(['data' => 'Error while translating text. Please try again.'], $exception->getCode());
        }

        return response(['data' => $data], 201);
    }

    private function checkInputData(array $data): ?Response
    {
        $rules = [
            'text' => 'required',
            'choices' => 'array|size:3',
            'choices.*.text' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if (!$validator->passes()) {
            return response($validator->getMessageBag(), 400);
        }

        return null;
    }
}
