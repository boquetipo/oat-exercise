<?php

namespace App\Models;

use App\Exceptions\FileAccessException;
use App\Services\Translator;

class FileRepositoryCsv extends Repository implements FileRepositoryInterface
{
    protected const FILENAME = 'questions.csv';

    public function __construct(?string $targetLang = null)
    {
        $this->targetLang = $targetLang;
        $this->translator = new Translator($targetLang);
    }

    /**
     * @return array
     */
    public function getQuestions(): array
    {
        $this->getFileContents();

        return $this->data;
    }

    /**
     * @param array $data
     * @return bool
     * @throws FileAccessException
     */
    public function addQuestion(array $data): bool
    {
        file_get_contents($this->getFilePath());

        $result = file_put_contents(
            $this->getFilePath(),
            $this->transformArrayToString($data),
            FILE_APPEND
        );

        if (false === $result) {
            throw new FileAccessException();
        }

        return true;
    }

    /**
     * Get data from csv file
     */
    public function getFileContents(): void
    {
        $file = fopen($this->getFilePath(), 'r');
        $this->data = [];
        $index = 1;

        while (($line = fgetcsv($file)) !== FALSE) {
            if (1 === $index++) {
                continue;
            }

            $this->data[] = $this->generateQuestion($line);
        }

        fclose($file);
    }

    /**
     * Get system path for json file
     *
     * @return string
     */
    public function getFilePath()
    {
        $path = [__DIR__, '..', '..', self::FILES_PATH, self::FILENAME];

        return implode(DIRECTORY_SEPARATOR, $path);
    }

    /**
     * Transform a simple array into an associative array
     *
     * @param array $line
     * @return array
     */
    private function generateQuestion(array $line): array
    {
        return [
            'text' => $this->translate($line[0]),
            'createdAt' => $line[1],
            'choices' => [
                ['text' => $this->translate($line[2])],
                ['text' => $this->translate($line[3])],
                ['text' => $this->translate($line[4])],
            ],
        ];
    }

    /**
     * Transforms an array into a CSV line
     *
     * @param array $data
     * @return string
     */
    private function transformArrayToString(array $data): string
    {
        $fields = [
            $this->wordWrap($data['text']),
            $this->wordWrap($data['createdAt']),
            $this->wordWrap($data['choices'][0]['text']),
            $this->wordWrap($data['choices'][1]['text']),
            $this->wordWrap($data['choices'][2]['text']),
        ];

        return implode(', ', $fields);
    }

    /**
     * Wrap a word around double quotes
     *
     * @param $string
     * @return string
     */
    private function wordWrap($string): string
    {
        return str_pad($string, strlen($string) + 2, '"', STR_PAD_BOTH);
    }
}
