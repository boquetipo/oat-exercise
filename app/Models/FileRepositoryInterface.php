<?php

namespace App\Models;

interface FileRepositoryInterface extends RepositoryInterface
{
    const FILES_PATH = 'files';

    /**
     * Get data from file
     */
    function getFileContents(): void;

    /**
     * Get system path for file
     *
     * @return string
     */
    function getFilePath();

    /**
     * Translates a single array
     *
     * @param string $text
     * @return string
     * @throws \App\Exceptions\TranslationErrorException
     */
    function translate(string $text): string;
}
