<?php

namespace App\Models;

use App\Exceptions\FileAccessException;
use App\Services\Translator;

class FileRepositoryJson extends Repository implements FileRepositoryInterface
{
    protected const FILENAME = 'questions.json';

    public function __construct(?string $targetLang = null)
    {
        $this->targetLang = $targetLang;
        $this->translator = new Translator($targetLang);
    }

    /**
     * @return array
     */
    public function getQuestions(): array
    {
        $this->getFileContents();

        return $this->data;
    }

    /**
     * @param array $data
     * @return bool
     * @throws FileAccessException
     */
    public function addQuestion(array $data): bool
    {
        $this->getFileContents();

        $result = file_put_contents(
            $this->getFilePath(),
            json_encode(array_merge($this->data, [$data]))
        );

        if (false === $result) {
            throw new FileAccessException();
        }

        return true;
    }

    /**
     * Get data from json file
     */
    public function getFileContents(): void
    {
        $contents = file_get_contents($this->getFilePath());
        $this->data = json_decode($contents, true);

        if (getenv('SOURCE_LANGUAGE') !== $this->targetLang) {
            foreach ($this->data as $key1 => $question) {
                foreach ($question as $key2 => $data) {
                    switch ($key2) {
                        case 'text':
                            $this->data[$key1][$key2] = $this->translate($data);

                            break;
                        case 'choices':
                            foreach ($data as $key3 => $choice) {
                                $this->data[$key1][$key2][$key3]['text'] = $this->translate($choice['text']);
                            }

                            break;
                    }
                }
            }
        }
    }

    /**
     * Get system path for json file
     *
     * @return string
     */
    public function getFilePath()
    {
        $path = [__DIR__, '..', '..', self::FILES_PATH, self::FILENAME];

        return implode(DIRECTORY_SEPARATOR, $path);
    }
}
