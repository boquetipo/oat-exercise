<?php

namespace App\Models;

use App\Services\Translator;

abstract class Repository
{
    protected array $data;

    protected ?string $targetLang;

    protected Translator $translator;

    /**
     * Translates a single array
     *
     * @param string $text
     * @return string
     * @throws \App\Exceptions\TranslationErrorException
     */
    public function translate(string $text): string
    {
        if (getenv('SOURCE_LANGUAGE') === $this->targetLang) {
            return $text;
        }

        return $this->translator->translate($text);
    }
}
