<?php

namespace App\Models;

use App\Exceptions\FileAccessException;

interface RepositoryInterface
{
    /**
     * @return array
     */
    function getQuestions(): array;

    /**
     * @param array $data
     * @return bool
     * @throws FileAccessException
     */
    function addQuestion(array $data): bool;
}
