<?php

namespace App\Services;

use App\Models\Repository;
use App\Models\FileRepositoryCsv;
use App\Models\FileRepositoryJson;

class RepositoryHandler
{
    protected Repository $repository;

    private const VALID_ORIGINS = [
        'JSON' => 'json',
        'CSV' => 'csv',
    ];

    private string $origin;

    public function __construct()
    {
        $this->origin = getenv('REPOSITORY');
    }

    /**
     * @param string|null $lang
     * @return array
     */
    public function getQuestions(?string $lang): array
    {
        switch ($this->origin) {
            case self::VALID_ORIGINS['JSON']:
                $this->repository = new FileRepositoryJson($lang);
                break;
            case self::VALID_ORIGINS['CSV']:
                $this->repository = new FileRepositoryCsv($lang);
                break;
        }

        return $this->repository->getQuestions();
    }

    /**
     * @param string[] $data
     * @return array
     */
    public function addQuestion(array $data): bool
    {
        switch ($this->origin) {
            case self::VALID_ORIGINS['JSON']:
                $this->repository = new FileRepositoryJson();
                break;
            case self::VALID_ORIGINS['CSV']:
                $this->repository = new FileRepositoryCsv();
                break;
        }

        if ($this->repository->addQuestion($data)) {
            return true;
        }

        return false;
    }
}
