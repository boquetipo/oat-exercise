<?php

namespace App\Services;

use App\Exceptions\TranslationErrorException;
use ErrorException;
use Stichoza\GoogleTranslate\GoogleTranslate;
use UnexpectedValueException;

class Translator
{
    protected string $targetLang;

    private const SOURCE_LANG = 'en';

    public function __construct(?string $targetLang = null)
    {
        $this->targetLang = $targetLang ?? getenv('SOURCE_LANGUAGE');
    }

    /**
     * Translates a single string
     *
     * @param string $text
     * @return array
     * @throws TranslationErrorException
     */
    public function translate(string $text): string
    {
        if (self::SOURCE_LANG === $this->targetLang) {
            return $text;
        }

        try {
            return GoogleTranslate::trans($text, $this->targetLang, self::SOURCE_LANG);
        } catch (ErrorException $exception) {
            throw new TranslationErrorException(503);
        } catch (UnexpectedValueException $exception) {
            throw new TranslationErrorException(400);
        }
    }
}
